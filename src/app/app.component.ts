import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'QR Code', url: '/folder/qrcode', icon: 'qr-code' },
    { title: 'Sobre nós', url: '/folder/about-us', icon: 'people' },
    { title: 'Rerportar bug', url: '/folder/bug', icon: 'bug' },
    { title: 'Sair do app', url: '/folder/exit', icon: 'exit' }
  ];
  constructor() {}
}
